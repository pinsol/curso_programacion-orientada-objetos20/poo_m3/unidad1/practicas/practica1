<html>
    
    <head>
        <title>Ejercicio 3 de la Practica 1 de php</title>
    </head>
    
    <body>
        <p>
            <?php
                echo "Hola mundo";
            ?>
        </p>
        
        <?php //cambiamos a php
            echo '<p>'; //volveremos a escribir sobre HTML de esta forma para optimizar
        ?>
        Hola mundo
        <?php //debemos cerrar el parrafo desde php
            echo '<p>';
        ?>
    </body>

</html>