<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <title>Ejercicio 2 de la practica 1 de php</title>
    </head>
    <body>
        
        <table width="100%" border="1">
            <tr>
                <td>
                    <?php
                        //Cuando se utiliza la instruccion echo se puede utilizar las comillas dobles o simples.
                        echo 'Este texto se escribe utilizando la función echo de php';
                    ?>
                </td>
                <td>
                    Aquí debe colocar un texto directamente en HTML
                </td>
            </tr>
            
            <tr>
                <td>
                    <?php
                        //Cuando se usa la instruccion print se puede utilizar las comillas dobles o simples.
                        print 'Este texto se escribe utilizando la función print de php';
                    ?>
                </td>
                <td>
                    <?php
                        echo "Academia Alpe";
                    ?>
                </td>
            </tr>
        </table>
        <br>
        <table width="100%" border="1">
            <tr>
                <td>
                    <?php
                        echo "Mi nombre es 'Pablo'"
                    ?>
                </td>
                <td>
                    <?php
                        echo 'Mi nombre es "Pablo"'
                    ?>
                </td>
            </tr>
        </table>
    </body>
</html>
