<html>
    <head>
        <title>Ejercicio 1 de la practica 1 de php</title>
    </head>
    <body>
        En este ejercicio vamos a escribir un texto en la pagina web a traves de php.
        <hr>
            <?php
                //Esto es un comentario

                    echo "Este texto está escrito desde el script de php.";

                /* Esto es otro comentario
                pero con la diferencia de que es de varias lineas */

                    echo "Este texto también se escribe desde el script de php.";
            ?>
        <hr>
            Esto está escrito en HTML normal
        <hr>
            <?php
                ############################################################
                ######## Este comentario es de una sola linea. #############
                ############################################################

                # En una pagina podemos colocar tantos scripts de php como se desee

                    print("Esta es otra forma de escribir cosas en la web");
            ?>

    </body>
</html>